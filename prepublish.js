const path = require('path');
const { promisify } = require('util');
const fs = require('fs');
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);
const childProcess = require('child_process');
const exec = promisify(childProcess.exec);

(async () => {
  const [ readmeTemplate, { stdout: usage } ] = await Promise.all([
    readFile(path.resolve(__dirname, '.README.template.md')),
    exec(path.resolve(__dirname, 'start.js') + ' --help'),
  ]);

  const readmeContent = readmeTemplate.toString()
    .replace('{{USAGE}}', usage);

  await writeFile(path.resolve(__dirname, 'README.md'), readmeContent);
})();
