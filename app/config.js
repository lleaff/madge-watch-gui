const program = require('commander');
const fromPairs = require('lodash/fromPairs');
const version = require('../package.json').version;

const ALLOWED_DIRECTIONS = ['LR', 'TB', 'BT', 'RL'];

// Copy paste from madge
program
  .name('madge-watch')
	.version(version)
	.usage('[options] <src...>')
	.option('-d --detached', 'execute as a background process', false)
	.option('-b, --basedir <path>', 'base directory for resolving paths')
	.option('-x, --exclude <regexp>', 'exclude modules using RegExp')
	.option('-l, --layout <name>', 'layout engine to use for graph (dot/neato/fdp/sfdp/twopi/circo)')
	.option('--extensions <list>', 'comma separated string of valid file extensions')
	.option('--require-config <file>', 'path to RequireJS config')
	.option('--webpack-config <file>', 'path to webpack config')
	.option('--include-npm', 'include shallow NPM modules', false)
	.option('--no-color', 'disable color in output and image', false)
  .option('--direction <dir>', `graph direction, one of: ${ALLOWED_DIRECTIONS.join(', ')}`)
  .option('--no-animations', `disable animations for added and modified files`, false)
	.option('--frame', 'use OS frame and titlebar instead of custom one', false)
	.parse(process.argv);

/*
 * https://www.npmjs.com/package/madge#configuration
 * baseDir           String  null  Base directory to use instead of the default
 * includeNpm        Boolean  false  If shallow NPM modules should be included
 * fileExtensions    Array  ['js']  Valid file extensions used to find files in directories
 * excludeRegExp     Array  false  An array of RegExp for excluding modules
 * requireConfig     String  null  RequireJS config for resolving aliased modules
 * webpackConfig     String  null  Webpack config for resolving aliased modules
 * layout            String  dot  Layout to use in the graph
 * rankdir           String  LR  Sets direction of the graph layout
 *
 * fontName          String  Arial  Font name to use in the graph
 * fontSize          String  14px  Font size to use in the graph
 * backgroundColor   String  #000000  Background color for the graph
 * nodeShape         String  box  A string specifying the shape of a node in the graph
 * nodeStyle         String  rounded  A string specifying the style of a node in the graph
 * nodeColor         String  #c6c5fe  Default node color to use in the graph
 * noDependencyColor String  #cfffac  Color to use for nodes with no dependencies
 * cyclicNodeColor   String  #ff6c60  Color to use for circular dependencies
 * edgeColor         String  #757575  Edge color to use in the graph
 * graphVizOptions   Object  false  Custom GraphViz options
 * graphVizPath      String  null  Custom GraphViz path
 * detectiveOptions  Object  false  Custom detective options for dependency-tree
 * dependencyFilter  Function  false  Function called with a dependency filepath (exclude substree by returning false)
 */
const madgeConfig = {};
if (program.basedir) {
  madgeConfig.baseDir = program.basedir;
}
if (program.exclude) {
  madgeConfig.excludeRegExp = [program.excludeRegExp];
}
if (program.layout) {
  madgeConfig.layout = program.layout;
}
if (program.extensions) {
  madgeConfig.fileExtensions = program.extensions.split(',').map((s) => s.trim());
}
if (program.requireConfig) {
  madgeConfig.requireConfig = program.requireConfig;
}
if (program.webpackConfig) {
  madgeConfig.webpackConfig = program.webpackConfig;
}
if (program.includeNpm) {
  madgeConfig.includeNpm = program.includeNpm;
}
if (!program.color) {
	madgeConfig.backgroundColor = '#111111';
	madgeConfig.nodeColor = '#ffffff';
	madgeConfig.noDependencyColor = '#ffffff';
	madgeConfig.cyclicNodeColor = '#ffffff';
	madgeConfig.edgeColor = '#cfcfcf';
} else {
  // madgeConfig.backgroundColor
  // madgeConfig.nodeColor
  // madgeConfig.noDependencyColor
  // madgeConfig.cyclicNodeColor
  madgeConfig.edgeColor = "#cfcfcf"
}
if (program.direction) {
  if (!ALLOWED_DIRECTIONS.includes(program.direction)) {
    console.log(program.helpInformation());
    process.exit(1);
  }
  madgeConfig.rankdir = program.direction;
}

module.exports = {
  program,
  madgeConfig,
}
