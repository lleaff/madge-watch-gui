#!/usr/bin/env node
const { execSync } = require('child_process');
const pkg = require('./package.json');
const chalk = require('chalk');
const os = require('os');

function commandSuccess(cmd) {
  try {
    execSync(cmd, { stdio: 'ignore', });
    return true;
  } catch(err) {
    return false;
  }
}

if (!commandSuccess('gvpr -V')) {
  let msg =
    chalk.red(`\n${chalk.bgWhite.black(pkg.name)}: Required dependency missing.\n` +
      `    ${chalk.bold('graphviz')} must be installed and the gvpr executable\n` + 
      `    accessible in system PATH.\n` +
      `    See ${chalk.reset('https://www.graphviz.org/download/')}` + chalk.red(` for installation\n` +
      `    instructions`));

  const platform = os.platform();
  let installCommand = null;
  if (platform === 'darwin') {
    installCommand = 'brew install graphviz || port install graphviz';
  } else if (platform === 'linux') {
    if (commandSuccess('apt-get --version')) {
      installCommand = 'apt-get install graphviz';
    }
  }
  if (installCommand) {
    msg += chalk.red(`, or directly try:\n`) +
      `    ${chalk.grey('$')} ${chalk.reset(installCommand)}`;
  } else {
    msg += chalk.red('.');
  }
  console.error(msg + '\n');
}
