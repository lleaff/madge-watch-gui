# madge-watch-gui [![npm](https://img.shields.io/npm/v/madge-watch-gui.svg)](https://www.npmjs.com/package/madge-watch-gui)

Display a dependency graph of your JavaScript code in a GUI and update it as files are modified.

Made with [Madge](https://github.com/pahen/madge), [Chokidar](https://github.com/paulmillr/chokidar) and [Electron](https://electronjs.org/).

## Usage

```
npm install -g madge-watch-gui

madge-watch my_project/index.js
```

<p align="center">
	<img alt="madge-watch-gui screenshot" src="https://gitlab.com/lleaff/madge-watch-gui/raw/master/screenshot.png" width="425">
</p>


```

  Usage: madge-watch [options] <src...>

  Options:

    -V, --version            output the version number
    -d --detached            execute as a background process
    -b, --basedir <path>     base directory for resolving paths
    -x, --exclude <regexp>   exclude modules using RegExp
    -l, --layout <name>      layout engine to use for graph (dot/neato/fdp/sfdp/twopi/circo)
    --extensions <list>      comma separated string of valid file extensions
    --require-config <file>  path to RequireJS config
    --webpack-config <file>  path to webpack config
    --include-npm            include shallow NPM modules
    --no-color               disable color in output and image
    --direction <dir>        graph direction, one of: LR, TB, BT, RL
    --no-animations          disable animations for added and modified files
    --frame                  use OS frame and titlebar instead of custom one
    -h, --help               output usage information

```

Madge works with Typescript too, but you have to point it directly at a `.ts` file, and not a `.js` file requiring `.ts` files.

## Dependencies

Requires [`graphviz`](https://www.graphviz.org/download/) to be installed.

**On Ubuntu**:
```
$ apt-get install graphviz
```
**On MacOS**:
```
$ brew install graphviz || port install graphviz
```

You can test the installation with:
```
$ gvpr -V
```

## Development
Show dev tools and print debug statements:
```
git clone https://gitlab.com/lleaff/madge-watch-gui.git
cd madge-watch-gui
NODE_ENV=development ./start test_files/index.js
```
Edit `.README.template.md`
