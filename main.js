const path = require('path');
const fs = require('fs');
const { program, madgeConfig } = require('./app/config');
const electron = require('electron')
const windowStateKeeper = require('electron-window-state');
const { app, BrowserWindow } = require('electron');
const once = require('lodash/once');
const debounce = require('lodash/debounce');
const throttle = require('lodash/throttle');
const takeWhile = require('lodash/takeWhile');
const isEqual = require('lodash/isEqual');
const { diff: arraysDiff } = require('fast-array-diff');
const chalk = require('chalk');
chalk.enabled = 1;

const DEVMODE = process.env.NODE_ENV === 'development';

const log = (...args) => console.log(...args);
log.debug = DEVMODE ? (...args) => console.log(...args) : ()=>{};

//------------------------------------------------------------

const fileArgs = program.args
if (!program.args.length) {
	console.log(program.helpInformation());
	process.exit(1);
}

function getBaseDir(entryPoints) {
  if (entryPoints.length <= 1) {
    return fs.lstatSync(entryPoints[0]).isDirectory() ?
      entryPoints[0] : path.dirname(entryPoints[0]);
  }
  const entries = entryPoints.map(entry => path.resolve(entry)).sort();
  const splitA = entries[0].split('/');
  const splitB = entries[1].split('/');
  const commonSegments = takeWhile(splitA, (segment, i) => splitB[i] === segment);
  const commonPath = commonSegments.join('/');
  return fs.lstatSync(commonPath).isDirectory() ?
    commonPath : path.dirname(commonPath);
}

const entrypoints = fileArgs.map(f => path.resolve(process.cwd(), f));
// Skip check for speed reasons
// for (const f of entrypoints) {
//   if (!fs.existsSync(entrypoints)) {
//     console.error(`File not found: "${fileArgs}" in "${process.cwd()}".`);
//     process.exit(1);
//   }
// }
let madgeBaseDir;
try {
  global.madgeBaseDir = program.basedir || getBaseDir(entrypoints);
  log(chalk.gray(`Watching with base directory: ${global.madgeBaseDir}`))
} catch(err) {
  if (err.code === 'ENOENT') {
    console.error(`File not found: "${err.path}".`);
    process.exit(1);
  } else {
    throw err;
  }
}

let watcher;

// =Electron setup
//------------------------------------------------------------

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

// Define global.windowSize
{
  let _windowSize = null;
  Object.defineProperty(global, 'windowSize', {
    get() {
      if (_windowSize === null) {
        try {
          const [ width, height ] = global.win.getSize();
          _windowSize = { width, height };
          return _windowSize;
        } catch(err) {
          return { width: 1, height: 1 };
        }
      } else {
        return _windowSize;
      }
    },
    set(value) {
      _windowSize = value;
    }
  });
}

function createWindow () {
  // Load the previous state with fallback to defaults
  const mainWindowState = windowStateKeeper({
    defaultWidth: 1000,
    defaultHeight: 800
  });

  // Create the browser window.
  const win = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    minWidth: 200,
    minHeight: 100,
    backgroundColor: '#111111',
    show: false,
    frame: program.frame || false,
    title: 'Module graph | ' + program.args.join(', '),
    icon: path.join(__dirname, 'assets/madge-electron-logo.png')
  });

  // Let us register listeners on the window, so we can update the state
  // automatically (the listeners will be removed when the window is closed)
  // and restore the maximized or full screen state
  mainWindowState.manage(win);

  win.once('ready-to-show', () => {
    win.show();
  });

  win.setMenu(null);

  // and load the index.html of the app.
  win.loadFile(__dirname + '/index.html');

  // Open the DevTools.
  if (DEVMODE) {
    win.webContents.openDevTools({ mode: 'undocked' });
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    global.win = null;
  });

  const pickFittingGraphFromResize = throttle(() => {
    if (!global.lastGraphs) {
      return;
    }
    const last = global.lastGraphs.images[0];
    const fitting = choseFittingImage(global.lastGraphs.images);
    if (last !== fitting) {
      sendChanges({ image: fitting.image });
    }
  }, 180, { trailing: true, })

  win.on('resize', () => {
    const [ width, height ] = win.getSize();
    global.windowSize = { width, height };
    pickFittingGraphFromResize();
  });

  global.win = win;
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', initialization);

// Quit when all windows are closed.
app.on('window-all-closed', async () => {
  app.quit();
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (global.win === null) {
    createWindow();
  }
});

//------------------------------------------------------------

const { ipcMain } = require('electron');
const madge = require('madge');
const chokidar = require('chokidar');

global.changed = new Set();

function normalizePath(pth) {
  return path.relative(global.madgeBaseDir, pth);
}

function sendChanges(opts={}) {
  const { image, added } = opts;
  log.debug(chalk.blue('file-changes: '), opts);
  const changed = [ ...global.changed.values() ].map(normalizePath);
  global.win.webContents.send('file-changes', { image, changed, added });
  global.changed.clear();
}

function updateState(data) {
  log.debug(chalk.blue('update-state: '), data);
  global.win.webContents.send('update-state', data);
}

const getImgFilename = () => `.madge-watch_tmp-${Math.random()*1e5|0}~.svg`

const normalizeChokidarWatchlist = (cWlist) => {
  return Object.entries(cWlist)
    .map(([dir, files]) => files.map(file => path.resolve(dir, file)))
    .reduce((p, files) => p.concat(files), [])
    // Remove directories
    .sort().filter((f, i, arr) => (i+1 > arr.length-1) || arr[i+1].indexOf(f) !== 0);
}

const normalizeMadgeWatchList = (mWlist) => {
  return mWlist.map(file => path.resolve(global.madgeBaseDir, file)).sort();
}

let lastGraphs;
ipcMain.on('get-last-data', (event, data) => {
  if (global.lastGraphs) {
    sendChanges({ image: global.lastGraphs.images[0], });
  }
  updateState({
    basedir: global.madgeBaseDir,
    animations: program.animations,
    frame: program.frame || false,
  });
});

function readImageSize(image) {
  const [ , w, h ] = image.toString().match(/<svg width="([^"]+)" height="([^"]+)"/);
  return { width: parseInt(w), height: parseInt(h), };
}

const ratio = ({ width, height }) => width / height;

function getRemainingArea(wind, img) {
  const fit = ratio(wind) > ratio(img) ?
    { width: img.width * (wind.height / img.height), height: wind.height } :
    { width: wind.width, height: img.height * (wind.width / img.width) };
  const remaining = wind.width * wind.height -
    fit.width * fit.height;
  return remaining;
}

function choseFittingImage(images) {
  images.sort((a, b) =>
    getRemainingArea(global.windowSize, a.size) -
    getRemainingArea(global.windowSize, b.size));
  return images[0];
}

const genMadgeWithDirection = direction => async (entryPoints, options) => {
  const madgeResult = await madge(
    entryPoints,
    { ...options, rankdir: direction, });

  const image = await madgeResult.svg();
  const imageSize = readImageSize(image);
  return { result: madgeResult, image, size: imageSize };
}

function hasGraphChanged(newGraph) {
  if (!global.lastGraphs) {
    return true;
  }
  const lastGraph = global.lastGraphs.graph;
  const hasChanged = !isEqual(lastGraph, newGraph);
  return hasChanged;
}

const generateGraph = debounce(async function generateGraph(entryPoints) {
  try {
    const madgeOptions = {
      ...madgeConfig,
    };

    const madgeResults = await Promise.all((madgeConfig.rankdir ?
      [ genMadgeWithDirection(madgeConfig.rankdir), ] :
      [
        genMadgeWithDirection('LR'),
        genMadgeWithDirection('TB')
      ]).map(gen => gen(entryPoints, madgeOptions)));

    const depGraph = await madgeResults[0].result.obj();

    if (!hasGraphChanged(depGraph)) {
      sendChanges();
      return null;
    }

    global.lastGraphs = {
      result: madgeResults[0].result,
      graph: depGraph,
      images: madgeResults.map(({ image, size }) => ({ image, size })),
    };

    const { result, image, size } = choseFittingImage(madgeResults);

    sendChanges({ image });

    const watchList = normalizeMadgeWatchList(Object.keys(depGraph));

    await updateWatcher(watchList);

    return watchList;
  } catch(err) {
    if (err.code === 'ENOENT') {
      console.error(`File not found: "${err.path}".`);
      process.exit(1);
    } else {
      console.error(err);
      throw err;
    }
  }
}, 200, { maxWait: 1000, });

const fileUpdateCallback = eventType => async function fileUpdateCallback(pth, stats) {
  log.debug(chalk.cyan(`[EVENT:${chalk.magenta(eventType)}] ${pth}`));

  global.changed.add(pth);

  generateGraph(entrypoints);
}

async function initialization() {
  await Promise.all([
    createWindow(),
  ]);
  const watchList = await generateGraph(entrypoints);
  global.watcher = await setupWatch(watchList);
}

//------------------------------

function updateWatcher(watchList) {
  const watchedFiles = normalizeChokidarWatchlist(global.watcher.getWatched())
  const { removed, added } = arraysDiff(watchedFiles, watchList);
  for (const toAdd of added) { global.watcher.add(toAdd); }
  for (const toRemove of removed) { global.watcher.unwatch(toRemove); }

  if (added.length) {
    const normalized = added.map(normalizePath);
    process.nextTick(() => {
      log(`${chalk.green(`Added ${chalk.yellow(normalized.length)} files to watchlist:`)}\n  ` +
          normalized.join(chalk.green(',\n  ')) + chalk.green('.'));
    });
    sendChanges({ added: normalized, });
  }
  if (removed.length) {
    const normalized = removed.map(normalizePath);
    process.nextTick(() => {
      log(`${chalk.red(`Removed ${chalk.yellow(normalized.length)} files from watchlist:`)}\n  ` +
          normalized.join(chalk.red(',\n  ')) + chalk.red('.'));
    });
  }
  return added;
}

function setupWatch(watchList) {
  const watcher = chokidar.watch(watchList);
  for (const eventType of ['change', 'unlink', 'unlinkDir']) {
    watcher.on(eventType, fileUpdateCallback(eventType));
  }
  return watcher;
}
