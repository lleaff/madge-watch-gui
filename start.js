#!/usr/bin/env node
// Swallow some cluttering output from electron

const path = require('path');
const spawn = require('child_process').spawn;
const electronPath = require('electron');

const IGNORE_PATTERNS = [
  /Gtk-WARNING/,
  /WARNING: You are currently running a version of TypeScript which is not officially supported by typescript-eslint-parser/,
  /* https://github.com/electron/electron/issues/12814 */
  /Electron \*\*\* WARNING: Textured window <[^>]*> is getting an implicitly transparent titlebar. This will break when linking against newer SDKs. Use NSWindow's -titlebarAppearsTransparent=YES instead./,
]

const haveOption = (args, { short, long }) => {
  const r = new RegExp(`^(-\\w*${short}\\w*|--${long})$`);
  return args.some(arg => r.test(arg));
}

const detached = haveOption(process.argv.slice(2), { short: 'd', long: 'detached' });

const execFileOptions = detached ? {
  shell: false,
  detached: true,
  stdio: ['ignore', 'pipe', 'pipe'],
} : {
  stdio: ['inherit', 'pipe', 'pipe'],
};

if (process.stdout.isTTY) {
  process.env.FORCE_COLOR = process.env.FORCE_COLOR || 'true';
}

const child = spawn(
  electronPath,
  [ path.resolve(__dirname, 'main.js'), ...process.argv.slice(2)],
  execFileOptions);

child.stdout.on('data', function(data) {
  if (IGNORE_PATTERNS.some(pattern => pattern.test(data))) {
    return;
  }
  process.stdout.write(data);
});
child.stderr.on('data', function(data) {
  if (IGNORE_PATTERNS.some(pattern => pattern.test(data))) {
    return;
  }
  process.stderr.write(data);
});

if (detached) {
  setTimeout(() => {
    process.exit(child.exitCode !== null ? child.exitCode : 0)
  }, 0.5e3)
} else {
  child.on('close', function(code) {
    process.exit(code);
  });
}
