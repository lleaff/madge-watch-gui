window.onerror = function(err){
  const errorDisplay = document.getElementById('error-display')
  const errorDisplayContent = document.getElementById('error-display-content')
  errorDisplayContent.innerHTML = err.stack || err.toString();
  errorDisplay.style.display = 'block';
}

const global = window;

global.startingUp = true;
setTimeout(() => {
  global.startingUp = false;
}, 1e3);

const EventEmitter = require('events');
const { remote, ipcRenderer } = require('electron');
const currentWindow = remote.getCurrentWindow();

// TODO
{
  let _state = {};
  const emitter = new EventEmitter();
  Object.defineProperty(global, 'state', {
    get: () => _state,
    set: (value) => {
      stateChanged(value);
      _state = value;
    },
  });
}


function stateChanged(state) {
  if ('frame' in state) {
    if (state.frame) {
      document.body.classList.remove('frameless');
    } else if (!state.frame) {
      document.body.classList.add('frameless');
    }
  }
}


// DOM
//------------------------------------------------------------
function renderGraph(image) {
  let imgContainer = document.getElementById('dependency-graph-container');
  imgContainer.innerHTML = image;

  // Build File <-> DOM node mapping
  const nodesMap = new Map(
    [ ...imgContainer.getElementsByClassName('node') ]
      .map(node => ([ node.querySelector('title').textContent, node ]))
  );
  global.fileNodeMap = nodesMap;
}

function animateHighlightNode(node) {
  if (startingUp || !global.state.animations) { return; }
  const TRANSITION_DELAY = 2.5;
  const clone = node.cloneNode(true);
  clone.setAttribute('id', clone.getAttribute('id') + '-' + (Math.random()*1e5|0));
  clone.classList.add('highlight-clone');
  clone.style.opacity = 1;
  clone.style.transition = TRANSITION_DELAY + 's';
  node.parentElement.insertBefore(clone, node);
  clone.setAttribute('filter', 'url(#filter-drop-shadow)');
  setTimeout(() => {
    clone.style.opacity = 0;
  }, 50);
  setTimeout(() => {
    clone.remove();
  }, TRANSITION_DELAY*1e3 + 100);
}

function highlightChanged(file) {
  const node = global.fileNodeMap.get(file);
  animateHighlightNode(node);
}

let _loading = false;
function showLoader() {
  _loading = true;
  const delay = 0.5e3;
  setTimeout(() => {
    if (_loading) {
      const el = document.querySelector('.loader');
      el.style.display = 'block';
    }
  }, delay)
}
function hideLoader() {
  const el = document.querySelector('.loader');
  el.style.display = 'none';
  _loading = false;
}

// Events
//------------------------------------------------------------
ipcRenderer.on('file-changes', (event, data) => {
  console.log('file-changes', data);
  hideLoader();
  if (!data) {
    const errMsg = `[file-changes] Didn't get data in message (${JSON.stringify(data)})`;
    throw new Error(errMsg);
  }
  if (data.image) {
    renderGraph(data.image);
  }
  if (data.changed && data.changed.length > 0) {
    setTimeout(() => {
      data.changed.forEach(highlightChanged);
    }, 0);
  }
  if (data.added && data.added.length > 0) {
    setTimeout(() => {
      data.added.forEach(highlightChanged);
    }, 0);
  }
});

ipcRenderer.send('get-last-data');

ipcRenderer.on('update-state', (event, data) => {
  console.log('update-state', data);
  global.state = { ...global.state, ...data };
});

// Setup window controls
//------------------------------------------------------------
const closeWindowBtn = document.querySelector('.close-window');
if (closeWindowBtn) {
  closeWindowBtn.addEventListener('click', btn => currentWindow.close());
}
const maximizeWindowBtn = document.querySelector('.maximize-window');
if (maximizeWindowBtn) {
  maximizeWindowBtn.addEventListener('click',
    btn => {
      if (currentWindow.isMaximized()) {
        currentWindow.unmaximize();
      } else {
        currentWindow.maximize();
      }
    });
}
const minimizeWindowBtn = document.querySelector('.minimize-window');
if (minimizeWindowBtn) {
  minimizeWindowBtn.addEventListener('click', btn => currentWindow.minimize());
}

const alwaysOnTopWindowBtn = document.querySelector('.always-on-top-window');
if (alwaysOnTopWindowBtn) {
  alwaysOnTopWindowBtn.addEventListener('click', btn => {
    alwaysOnTopWindowBtn.classList.toggle('active');
    currentWindow.setAlwaysOnTop(!currentWindow.isAlwaysOnTop())
  });
}
